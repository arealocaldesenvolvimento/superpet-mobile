import React from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'

const style = StyleSheet.create({
  box: {
    width: Dimensions.get('screen').width - 32,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
    height: 170,
    backgroundColor: '#FFFFFFCC',
    marginBottom: 45,
    borderRadius: 25,
    overflow: 'hidden'
  }
})

export const Box = ({ children }) => <View style={style.box}>{children}</View>
