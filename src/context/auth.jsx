import React, { useState, createContext, useLayoutEffect, useCallback } from 'react'
import { AsyncStorage, ActivityIndicator, View } from 'react-native'
import http from '../api/http'
import { createTable } from '../services/sqlite'

export const AuthContext = createContext()

export const AuthProvider = ({ children }) => {
  const [signed, setSigned] = useState(false)
  const [user, setUser] = useState()
  const [bootstraped, setBootstraped] = useState(true)

  async function handleSignIn(data) {
    const { token } = data
    const { user } = data

    await AsyncStorage.setItem('token', token)
    await AsyncStorage.setItem('user', JSON.stringify(user))

    setUser(user)
    createTable()
    setSigned(true)
  }

  async function handleSignOut() {
    await AsyncStorage.clear()
    setSigned(false)
  }

  const bootstrap = useCallback(() => {
    http
      .get('/auth')
      .then(async (response) => {
        await AsyncStorage.setItem('token', response.data.token)
        await AsyncStorage.setItem('user', JSON.stringify(response.data.user))
        setUser(response.data.user)
        createTable()
        setSigned(true)
        setBootstraped(false)
      })
      .catch((err) => {
        // setSigned(false)
        setBootstraped(false)
      })
  }, [])

  useLayoutEffect(() => {
    bootstrap()
  }, [bootstrap])

  if (bootstraped) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator color="#305F72" size="large" />
      </View>
    )
  }

  return <AuthContext.Provider value={{ signed, user, handleSignIn, handleSignOut }}>{children}</AuthContext.Provider>
}
