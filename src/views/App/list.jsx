import React, { useState, useContext, useEffect, useCallback } from 'react'
import NetInfo from '@react-native-community/netinfo'
import { Text, View, Modal, SafeAreaView, FlatList } from 'react-native'
import { style } from './style.css'
import http from '../../api/http'

import { AuthContext } from '../../context/auth'
import { BackButtonNav } from '../../components/layout/BackButtonNav'
import { Loading } from '../../components/Loading'
import UsertItem from '../../components/user/UserItem'
import { Error } from '../../components/RenderError'
import { fetchLocalData, deleteItem } from '../../services/sqlite'
import { addNumberToUser } from '../../helpers'
import { Button } from '../../components/layout/Button'
import { BottomSheet } from 'react-native-btr'

const List = ({ navigation }) => {
  const { user } = useContext(AuthContext)
  const { _id } = user

  const [loadedData, setLoadedData] = useState([])
  const [isOffline, setOfflineStatus] = useState(false)
  const [isLoading, setLoading] = useState(false)

  const fetchUserNumbers = async () => {
    const response = await http.get(`/users/${_id}`)
    const { nr_inscricao } = response.data
    return nr_inscricao
  }

  const fetchSubscriptionData = async (array) => {
    let data = []

    const response = await http.get(`/contacts/inscricao/${array}`)
    for (const key in response.data) {
      if (response.data[key].length > 0) {
        const result = response.data[key]
        for (const key in result) {
          data = [...data, result[key]]
        }
      }
    }

    data.sort(function (a, b) {
      if (a.Nome > b.Nome) {
        return 1
      }
      if (b.Nome > a.Nome) {
        return -1
      }
      return 0
    })

    return data
  }

  const fetchData = useCallback(async () => {
    setLoading(true)

    try {
      const localData = await fetchLocalData()
      if (localData.length > 0) {
        localData.map(async ({ id, value }) => {
          await addNumberToUser(value, user)
          await deleteItem(id)
        })
      }
      const numbers = await fetchUserNumbers()
      if (numbers.length > 0) {
        const data = await fetchSubscriptionData(numbers)
        setLoadedData(data)
      }
      isOffline && setOfflineStatus(false)
      setLoading(false)
    } catch (error) {
      setLoading(false)
      setLoadedData([])
    }
  }, [isOffline])

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable)
      setOfflineStatus(offline)
    })

    fetchData()

    return () => removeNetInfoSubscription()
  }, [])

  const NoInternetModal = ({ show }) => (
    <BottomSheet visible={show} style={style.modal}>
      <View style={style.modalContainer}>
        <Text style={style.modalTitle}>Erro de conexão</Text>
        <Text style={style.modalText}>Oops! Parece que seu dispositivo não está conectado a internet.</Text>

        <Button
          onPress={() => {
            navigation.popToTop()
          }}
        >
          <Text style={style.buttonText}>Voltar</Text>
        </Button>
      </View>
    </BottomSheet>
  )

  const rendeList = () => (
    <>
      <FlatList
        onRefresh={fetchData}
        refreshing={isLoading}
        style={{ width: '100%', paddingTop: 10, marginBottom: 20 }}
        data={loadedData}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={
          <Error>
            <Button onPress={fetchData}>
              <Text style={style.buttonText}> Atualizar </Text>
            </Button>
          </Error>
        }
        keyExtractor={(__, i) => JSON.stringify(i)}
        renderItem={({ item, index }) => <UsertItem user={item} index={index} />}
      />
    </>
  )

  return (
    <>
      <SafeAreaView style={[style.container, { alignItems: 'center' }]}>
        <BackButtonNav />
        <Text style={{ marginBottom: 20, marginTop: 22, fontSize: 20, color: '#305F72', fontWeight: 'bold' }}>
          Dados de visitantes
        </Text>
        {isLoading ? <Loading /> : rendeList()}
        <NoInternetModal show={isOffline} />
      </SafeAreaView>
    </>
  )
}

export default List
