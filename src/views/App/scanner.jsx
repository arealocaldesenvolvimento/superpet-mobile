import React, { useState, useEffect, useContext } from 'react'
import {
  ActivityIndicator,
  ImageBackground,
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Image,
  Dimensions
} from 'react-native'
import NetInfo from '@react-native-community/netinfo'
import { BarCodeScanner } from 'expo-barcode-scanner'
import { style } from './style.css'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import border from '../../../assets/border.png'
import background from '../../../assets/background.jpg'
import image from '../../../assets/no-camera.png'
import { BackButtonNav } from '../../components/layout/BackButtonNav'
import { Box } from '../../components/layout/Box'
import { AuthContext } from '../../context/auth'
import SnackContext from '../../context/snack'
import { newItem } from '../../services/sqlite'
import { addNumberToUser } from '../../helpers'

const Scanner = ({ navigation }) => {
  const { changeSnackState } = useContext(SnackContext)
  const { user } = useContext(AuthContext)

  const [hasPermission, setHasPermission] = useState(null)
  const [scanned, setScanned] = useState(false)
  const [isOffline, setOfflineStatus] = useState(false)

  useEffect(() => {
    ;(async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable)
      setOfflineStatus(offline)
    })

    return () => removeNetInfoSubscription()
  }, [])

  const handleBarCodeScanned = async ({ __, data }) => {
    setScanned(true)

    if (!isOffline) {
      const added = await addNumberToUser(data, user)
      if (added) {
        changeSnackState(`Número de inscrição: ${data}`)
      } else {
        changeSnackState(`Código já registrado: ${data}`, false, 'orange')
      }
    } else {
      newItem(data)
      changeSnackState(`Armazenado localmente: ${data}`)
    }

    navigation.navigate('List')
  }

  if (hasPermission === null) {
    return (
      <View style={style.container}>
        <ImageBackground style={style.ImageBackground} source={background}>
          <View style={style.main}>
            <ActivityIndicator size="large" color="white" />
            <Text style={{ marginTop: 20, color: 'white', fontSize: 20 }}>Solicitando acesso a câmera</Text>
          </View>
        </ImageBackground>
      </View>
    )
  }
  if (hasPermission === false) {
    return (
      <View style={style.container}>
        <ImageBackground style={style.ImageBackground} source={background}>
          <View style={style.main}>
            <Image source={image} />
            <Text style={{ marginTop: 20, color: 'white', fontSize: 20 }}>Sem acesso a câmera</Text>
          </View>
        </ImageBackground>
      </View>
    )
  }

  return (
    <>
      <SafeAreaView>
        <BarCodeScanner
          style={[
            { width: Dimensions.get('screen').width, height: Dimensions.get('screen').height },
            StyleSheet.absoluteFill,
            style.container,
            { backgroundColor: '#009EAA' }
          ]}
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        >
          <View style={[style.content, { flex: 1 }]}>
            <BackButtonNav />
            <Box>
              <Text style={style.textBox}>APONTE SUA CÂMERA PARA O QRCODE</Text>
              <MaterialIcons name="qr-code" size={120} color="#a3bdbe" />
            </Box>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                height: 300
              }}
            >
              <Image source={border} />
            </View>
          </View>
        </BarCodeScanner>
      </SafeAreaView>
    </>
  )
}

export default Scanner
