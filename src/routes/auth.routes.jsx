import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Login from '../views/Auth/login'
import Register from '../views/Auth/register'

const AuthRoutes = () => {
  const AuthStack = createStackNavigator()

  return (
    <>
      <AuthStack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        <AuthStack.Screen name="Login" component={Login} />
        <AuthStack.Screen name="Register" component={Register} />
      </AuthStack.Navigator>
    </>
  )
}

export default AuthRoutes
