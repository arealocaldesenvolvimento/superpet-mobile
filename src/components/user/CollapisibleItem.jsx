import React, { useState, useEffect } from 'react'
import { Dimensions } from 'react-native'
import Collapsible from 'react-native-collapsible'

import { Available, Unavailable } from './Header'
import { UserData } from './UserData'

export const CollapsibleItem = ({ ...props }) => {
  const [collapsed, setCollapsed] = useState(true)
  const [blocked, setBlocked] = useState(false)
  const { Nome } = props.user
  const { index } = props

  useEffect(() => {
    setBlocked(index > 8)
  }, [])

  const toggleExpanded = () => {
    setCollapsed(!collapsed)
  }

  return (
    <>
      {blocked ? <Unavailable name={Nome} /> : <Available onPress={toggleExpanded} name={Nome} />}
      <Collapsible
        collapsed={collapsed}
        align="center"
        style={{ width: Dimensions.get('screen').width - 80, alignItems: 'flex-start' }}
      >
        <UserData {...props} />
      </Collapsible>
    </>
  )
}
