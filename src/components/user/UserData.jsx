import React from 'react'
import { View, Text, ToastAndroid, TouchableOpacity } from 'react-native'
import { formatWithMask, Masks } from 'react-native-mask-input'
import Clipboard from 'expo-clipboard'

const format = (value) => {
  const { masked } = formatWithMask({
    text: value,
    mask: Masks.BRL_PHONE,
    obfuscationCharacter: '-'
  })

  return masked
}

export const UserData = ({ user }) => {
  const copyToClipboard = (value) => {
    Clipboard.setString(value)
    ToastAndroid.show('copiado!', ToastAndroid.SHORT)
  }

  return (
    <>
      <View style={{ marginBottom: 15 }}>
        <View style={{ paddingTop: 20 }}>
          <Text style={{ fontSize: 12, color: '#6A6A6A' }}>Nome</Text>
          <Text style={{ color: '#305F72', fontSize: 18, fontWeight: 'bold' }}>{user.Nome}</Text>
        </View>

        {user.Fantasia != '' && (
          <View style={{ paddingTop: 20 }}>
            <Text style={{ fontSize: 12, color: '#6A6A6A' }}>Empresa</Text>
            <Text style={{ color: '#305F72', fontSize: 18, fontWeight: 'bold' }}>{user.Fantasia}</Text>
          </View>
        )}

        {user.Fone != '' && (
          <View style={{ paddingTop: 20 }}>
            <Text style={{ fontSize: 12, color: '#6A6A6A' }}>Fone</Text>
            <TouchableOpacity onPress={() => copyToClipboard(format(user.Fone))}>
              <Text style={{ color: '#305F72', fontSize: 18, fontWeight: 'bold' }}>{format(user.Fone)}</Text>
            </TouchableOpacity>
          </View>
        )}

        {user.Email != '' && (
          <View style={{ paddingTop: 20 }}>
            <Text style={{ fontSize: 12, color: '#6A6A6A' }}>E-mail</Text>
            <TouchableOpacity onPress={() => copyToClipboard(format(user.Fone))}>
              <Text style={{ color: '#305F72', fontSize: 18, fontWeight: 'bold' }}>{user.Email}</Text>
            </TouchableOpacity>
          </View>
        )}

        <View style={{ paddingTop: 20 }}>
          <Text style={{ fontSize: 12, color: '#6A6A6A' }}>Tipo</Text>
          <Text style={{ color: '#305F72', fontSize: 18, fontWeight: 'bold' }}>{user.Tipo}</Text>
        </View>

        {user.Cidade != '' && (
          <View style={{ paddingTop: 20 }}>
            <Text style={{ fontSize: 12, color: '#6A6A6A' }}>Cidade</Text>
            <Text style={{ color: '#305F72', fontSize: 18, fontWeight: 'bold' }}>{user.Cidade}</Text>
          </View>
        )}
      </View>
    </>
  )
}
