import React from 'react'
import { StatusBar } from 'react-native'

const focusAwareStatusBar = () => <StatusBar barStyle="light-content" backgroundColor="#0f9eaa" />

export default focusAwareStatusBar
