import React, { useContext } from 'react'
import SnackBar from 'react-native-snackbar-component'

import SnackContext from '../context/snack'

const SnackbarComponent = () => {
  const { state, text, error, color } = useContext(SnackContext)

  return (
    <>
      {error ? (
        <SnackBar
          containerStyle={{
            zIndex: 1,
            marginHorizontal: 10,
            borderRadius: 5
          }}
          autoHidingTime={3000}
          visible={state}
          position="bottom"
          textMessage={text}
          backgroundColor="red"
          bottom={30}
        />
      ) : (
        <SnackBar
          containerStyle={{
            zIndex: 1,
            marginHorizontal: 10,
            borderRadius: 5
          }}
          autoHidingTime={3000}
          visible={state}
          position="bottom"
          textMessage={text}
          backgroundColor={color}
          bottom={30}
        />
      )}
    </>
  )
}

export default SnackbarComponent
