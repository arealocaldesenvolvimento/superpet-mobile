import React, { useState, useContext } from 'react'
import { ScrollView, View, Text, TextInput, CheckBox, TouchableOpacity, ActivityIndicator } from 'react-native'
import Moment from 'moment'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { style } from './style.css'

import http from '../../api/http'

import { BackButtonNav } from '../../components/layout/BackButtonNav'
import { Button } from '../../components/layout/Button'
import { ButtonLoading } from '../../components/layout/ButtonLoading'
import { AuthContext } from '../../context/auth'
import SnackContext from '../../context/snack'

const Register = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState([])
  const [isSelected, setSelection] = useState(false)

  const { changeSnackState } = useContext(SnackContext)
  const { handleSignIn } = useContext(AuthContext)

  const handleData = (value, name) => {
    setData({ ...data, [name]: value })
  }

  const authenticate = async ({ email, password }) => {
    const response = await http.post('auth/', { email, password })
    handleSignIn(response.data)
  }

  const register = async () => {
    setLoading(true)
    http
      .post('/users', { ...data })
      .then((response) => {
        const message = 'Cadastrado com sucesso!'
        changeSnackState(message)
        authenticate(response.data)
      })
      .catch((err) => {
        setLoading(false)
        console.log(err.response.data)
        changeSnackState('Não foi possível registrar o usuário', true)
      })
  }

  return (
    <>
      <ScrollView>
        <View style={style.registerContainer}>
          <BackButtonNav />
          <Text style={{ marginTop: 70, color: '#707070', fontSize: 19 }}>
            Complete seu cadastro inserindo seus dados abaixo.
          </Text>
          <View style={style.registerForm}>
            <TextInput
              onChangeText={(value) => handleData(value, 'name')}
              style={style.registerInput}
              placeholderTextColor="#305F72"
              placeholder="nome"
            />

            <TextInput
              style={style.registerInput}
              placeholderTextColor="#305F72"
              placeholder="email"
              keyboardType="email-address"
              onChangeText={(value) => handleData(value, 'email')}
            />

            <TextInput
              secureTextEntry
              style={style.registerInput}
              placeholderTextColor="#305F72"
              placeholder="senha"
              onChangeText={(value) => handleData(value, 'password')}
            />

            <View style={style.terms}>
              <CheckBox value={isSelected} onValueChange={setSelection} style={style.checkbox} />
              <Text style={style.label}>mensagem de concordo com os termos de serviço e política de privacidade.?</Text>
            </View>

            {loading ? (
              <ButtonLoading>
                <ActivityIndicator size="large" color="#FFF" />
              </ButtonLoading>
            ) : (
              <Button onPress={register}>
                <Text style={{ color: '#fff' }}>SALVAR DADOS</Text>
                <MaterialIcons name="east" size={13} color="#FFF" />
              </Button>
            )}
          </View>
        </View>
      </ScrollView>
    </>
  )
}

export default Register
