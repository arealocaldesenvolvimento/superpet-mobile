import React from 'react'
import { StyleSheet, Text } from 'react-native'

const style = StyleSheet.create({
  text: {
    color: '#FFF',
    fontSize: 20
  },
  name: {
    fontSize: 25,
    color: '#FFF',
    fontWeight: 'bold',
    marginBottom: 35
  }
})

export const Greetings = ({ name }) => {
  const today = new Date()
  const curHr = today.getHours()

  const Component = () => {
    if (curHr < 12) {
      return (
        <>
          <Text style={style.text}>Bom dia,</Text>
          <Text style={style.name}> {name} </Text>
        </>
      )
    } else if (curHr < 18) {
      return (
        <>
          <Text style={style.text}>Boa tarde,</Text>
          <Text style={style.name}> {name} </Text>
        </>
      )
    } else {
      return (
        <>
          <Text style={style.text}>Boa noite,</Text>
          <Text style={style.name}> {name} </Text>
        </>
      )
    }
  }

  return <Component />
}
