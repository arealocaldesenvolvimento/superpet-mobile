import React from 'react'
import { View, Text, Dimensions } from 'react-native'

export const Error = ({ children }) => (
  <>
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <View
        style={{
          marginTop: 20,
          paddingHorizontal: 30,
          borderTopWidth: 1,
          borderBottomWidth: 1,
          marginHorizontal: 30,
          borderColor: '#adc4c5'
        }}
      >
        <Text
          style={{ marginTop: 30, marginBottom: 30, justifyContent: 'center', color: '#305F72', fontWeight: 'bold' }}
        >
          ops. ainda não temos dados para exibir. tente novamente.
        </Text>
      </View>

      {children}
    </View>
  </>
)
