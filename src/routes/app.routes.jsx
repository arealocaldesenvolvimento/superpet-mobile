import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Main from '../views/App/main'
import Scanner from '../views/App/scanner'
import List from '../views/App/list'
import Settings from '../views/App/settings'
import Profile from '../views/App/profile'

const AppRoutes = () => {
  const AppStack = createStackNavigator()

  return (
    <>
      <AppStack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        <AppStack.Screen name="Main" component={Main} />
        <AppStack.Screen name="Scanner" component={Scanner} />
        <AppStack.Screen name="List" component={List} />
        <AppStack.Screen name="Settings" component={Settings} />
        <AppStack.Screen name="Profile" component={Profile} />
      </AppStack.Navigator>
    </>
  )
}

export default AppRoutes
