import FocusAwareStatusBar from './src/components/FocusAwareStatusBar'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { AuthProvider } from './src/context/auth'
import { SnackProvider } from './src/context/snack'

import Routes from './src/routes'

export default function App() {
  return (
    <NavigationContainer>
      <FocusAwareStatusBar />
      <AuthProvider>
        <SnackProvider>
          <Routes />
        </SnackProvider>
      </AuthProvider>
    </NavigationContainer>
  )
}
