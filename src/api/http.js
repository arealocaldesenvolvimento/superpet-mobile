import axios from 'axios'
import { AsyncStorage } from 'react-native'

const http = axios.create({
  // baseURL: 'http://52.7.124.222' backend v2
  baseURL: 'http://54.161.158.197'
  // baseURL: 'http://192.168.0.100:3000'
})

http.interceptors.response.use(function (response) {
  if (response && response.data && response.data.access_token) {
    AsyncStorage.setItem('token', response.data.access_token)
  }
  return response
})

http.interceptors.request.use(async function (request) {
  const access_token = await AsyncStorage.getItem('token')
  if (access_token) {
    request.headers.Authorization = `Bearer ${access_token}`
  }
  return request
})

export default http
