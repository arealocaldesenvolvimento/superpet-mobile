import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'

const style = StyleSheet.create({
  buttonLoading: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 13,
    paddingHorizontal: 25,
    borderRadius: 5,
    backgroundColor: '#305F72',
    height: 55
  }
})

export const ButtonLoading = ({ children, ...props }) => (
  <TouchableOpacity style={style.buttonLoading} {...props}>
    {children}
  </TouchableOpacity>
)
