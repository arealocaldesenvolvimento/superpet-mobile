import React, { useState, useContext, useLayoutEffect } from 'react'
import {
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  ActivityIndicator
} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import logo from '../../../assets/logo.png'
import http from '../../api/http'
import background from '../../../assets/background.jpg'
import { style } from './style.css'
import { AuthContext } from '../../context/auth'
import { Button } from '../../components/layout/Button'
import { ButtonLoading } from '../../components/layout/ButtonLoading'

import SnackContext from '../../context/snack'

const Login = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()
  const { handleSignIn } = useContext(AuthContext)
  const { changeSnackState } = useContext(SnackContext)

  const login = () => {
    setLoading(true)

    http
      .post('auth/', { email, password })
      .then((response) => {
        changeSnackState('Logado com Sucesso')
        handleSignIn(response.data)
      })
      .catch((err) => {
        setLoading(false)
        changeSnackState('Email ou senha inválidos!', true)
      })
  }

  return (
    <>
      <ImageBackground style={style.ImageBackground} source={background}>
        <ScrollView>
          <View style={style.container}>
            <Image style={style.logo} source={logo} />
            <View style={style.form}>
              <View style={style.login}>
                <TextInput
                  placeholderTextColor="#305F72"
                  keyboardType="email-address"
                  style={style.input}
                  placeholder="e-mail"
                  name="email"
                  onChangeText={setEmail}
                />
                <TextInput
                  placeholderTextColor="#305F72"
                  secureTextEntry
                  autoCompleteType="password"
                  style={style.input}
                  placeholder="senha"
                  name="password"
                  onChangeText={setPassword}
                />

                {loading ? (
                  <ButtonLoading>
                    <ActivityIndicator size="large" color="#FFF" />
                  </ButtonLoading>
                ) : (
                  <Button onPress={login}>
                    <Text style={{ color: '#fff' }}>ENTRAR</Text>
                    <MaterialIcons name="east" size={13} color="#FFF" />
                  </Button>
                )}
              </View>

              <View style={style.register}>
                <Text style={style.text}>Ainda não tem cadastro ?</Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('Register')
                  }}
                  style={style.buttonRegister}
                >
                  <Text style={{ color: '#fff' }}>CADASTRE-SE</Text>
                  <MaterialIcons name="east" size={13} color="#FFF" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </>
  )
}

export default Login
