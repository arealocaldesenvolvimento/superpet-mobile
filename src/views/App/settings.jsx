import React, { useContext } from 'react'
import { SafeAreaView, ImageBackground, View, Text, TouchableOpacity } from 'react-native'
import { style } from './style.css'
import background from '../../../assets/background.jpg'
import { AuthContext } from '../../context/auth'
import { BackButtonNav } from '../../components/layout/BackButtonNav'
import { Button } from '../../components/layout/Button'

const Settings = ({ navigation }) => {
  const { handleSignOut } = useContext(AuthContext)

  return (
    <SafeAreaView style={style.container}>
      <BackButtonNav />
      <ImageBackground style={style.ImageBackground} source={background}>
        <View style={{ marginTop: 50 }}>
          <View style={{ width: '100%', paddingHorizontal: 20 }}>
            <Button
              onPress={() => {
                navigation.navigate('Profile')
              }}
            >
              <Text style={style.buttonText}>MEUS DADOS</Text>
            </Button>

            <Button
              onPress={() => {
                handleSignOut()
              }}
            >
              <Text style={style.buttonText}>SAIR DA CONTA</Text>
            </Button>
          </View>
        </View>
      </ImageBackground>
    </SafeAreaView>
  )
}

export default Settings
