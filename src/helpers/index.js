import http from '../api/http'

export const addNumberToUser = async (value, user) => {
  const { _id } = user
  const findUser = await http.get(`/users/${_id}`)
  const { nr_inscricao } = findUser.data

  const exists = nr_inscricao.filter((item) => value === item)
  if (exists != '') {
    return false
  } else {
    await http.put(`/users/${_id}`, {
      ...user,
      nr_inscricao: value
    })
    return true
  }
}
