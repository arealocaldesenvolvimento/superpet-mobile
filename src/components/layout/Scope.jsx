import React from 'react'
import { SafeAreaView } from 'react-native'
import SnackbarComponent from '../SnackBar'

export const Scope = ({ children }) => {
  return (
    <>
      <SafeAreaView style={{ flex: 1, zIndex: -1 }}>
        <SnackbarComponent />
        {children}
      </SafeAreaView>
    </>
  )
}
