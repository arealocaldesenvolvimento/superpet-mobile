import React, { useContext } from 'react'
import { SafeAreaView, ImageBackground, View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'

import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Icon from 'react-native-vector-icons/FontAwesome'
import logo from '../../../assets/logoMain.png'
import background from '../../../assets/background.jpg'
import { style } from './style.css'
import { Greetings } from '../../components/Greetings'
import { Box } from '../../components/layout/Box'

import { AuthContext } from '../../context/auth'

const Main = ({ navigation }) => {
  const { user } = useContext(AuthContext)

  const navigate = () => {
    navigation.navigate('Scanner')
  }

  return (
    <>
      <SafeAreaView style={style.container}>
        <ImageBackground style={style.ImageBackground} source={background}>
          <ScrollView>
            <View style={style.main}>
              <TouchableOpacity
                style={{
                  top: 20,
                  right: 15,
                  position: 'absolute',
                  backgroundColor: '#4dadb4',
                  padding: 5,
                  borderRadius: 5,
                  zIndex: 10
                }}
                onPress={() => navigation.navigate('Settings')}
              >
                <MaterialIcons name="menu" size={30} color="#FFF" />
              </TouchableOpacity>

              <Image style={style.logo} source={logo} />

              <Greetings name={user.name} />

              <View style={style.content}>
                <Box>
                  <View style={{ marginRight: 10 }}>
                    <Text style={style.textBox}>Clique aqui para ler o QR Code</Text>
                    <TouchableOpacity onPress={() => navigate()} style={style.cta}>
                      <Text style={{ color: '#FFF' }}>ABRIR CAMERA</Text>
                      <MaterialIcons name="east" size={13} color="#FFF" />
                    </TouchableOpacity>
                  </View>
                  <MaterialIcons name="qr-code" size={120} color="#a3bdbe" />
                </Box>

                <Box>
                  <View style={{ marginRight: 30 }}>
                    <Text style={style.textBox}>Veja aqui as Infos dos Participantes</Text>
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('List')
                      }}
                      style={style.cta}
                    >
                      <Text style={{ color: '#FFF' }}>LISTAR</Text>
                      <MaterialIcons name="east" size={13} color="#FFF" />
                    </TouchableOpacity>
                  </View>
                  <Icon name="user" size={120} color="#a3bdbe" />
                </Box>
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
      </SafeAreaView>
    </>
  )
}

export default Main
