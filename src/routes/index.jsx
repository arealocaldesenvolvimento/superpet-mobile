import React, { useContext, useMemo } from 'react'
import AuthRoutes from './auth.routes'
import AppRoutes from './app.routes'
import { AuthContext } from '../context/auth'
import { Scope } from '../components/layout/Scope'

const Routes = () => {
  const { signed } = useContext(AuthContext)

  const Component = useMemo(() => {
    if (!signed) {
      return AuthRoutes
    }
    return AppRoutes
  }, [signed])

  return (
    <>
      <Scope>
        <Component />
      </Scope>
    </>
  )
}

export default Routes
