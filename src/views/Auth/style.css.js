import { StyleSheet } from 'react-native'

export const style = StyleSheet.create({
  /**
   * Login component
   */
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  ImageBackground: {
    width: '100%',
    height: '100%'
  },

  logo: {
    resizeMode: 'contain',
    width: '100%',
    height: 250,
    marginVertical: 50
  },

  form: {
    width: '100%',
    marginHorizontal: 20,
    paddingHorizontal: 20
  },

  login: {
    borderBottomColor: '#29adb7',
    paddingBottom: 30,
    borderBottomWidth: 2
  },

  input: {
    fontSize: 14,
    backgroundColor: '#fff',
    paddingVertical: 13,
    paddingHorizontal: 25,
    borderRadius: 5,
    marginBottom: 20
  },

  register: {
    width: '100%',
    paddingVertical: 20,
    position: 'relative'
  },

  text: {
    fontSize: 20,
    color: '#FFF',
    textAlign: 'center',
    marginBottom: 20
  },

  buttonRegister: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 13,
    paddingHorizontal: 25,
    borderRadius: 5,
    backgroundColor: '#40bf70',
    height: 55
    // paddingBottom: 100
  },

  contentContainer: {
    flex: 1,
    alignItems: 'center'
  },

  /**
   * Register component
   */

  registerContainer: {
    paddingHorizontal: 20,
    marginBottom: 30
  },

  registerForm: {
    marginTop: 23
  },

  terms: {
    flexDirection: 'row',
    marginBottom: 30,
    paddingRight: 15
  },

  registerInput: {
    borderWidth: 2,
    borderColor: '#21ABB5',
    paddingVertical: 13,
    paddingHorizontal: 25,
    borderRadius: 10,
    marginBottom: 9
  },

  picker: {
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 2,
    borderColor: '#21ABB5',
    paddingVertical: 13,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginBottom: 9,
    height: 55
  },

  datePicker: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#21ABB5',
    paddingVertical: 13,
    paddingHorizontal: 19,
    borderRadius: 10,
    marginBottom: 9
  },

  checkbox: {},
  label: {}
})
