import React from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { useNavigation } from '@react-navigation/native'

export const BackButtonNav = () => {
  const navigation = useNavigation()

  const goBack = () => {
    navigation.popToTop()
  }

  return (
    <>
      <TouchableOpacity style={style.button} onPress={goBack}>
        <View>
          <Icon name="arrow-left" size={20} color="white" />
        </View>
      </TouchableOpacity>
    </>
  )
}

const style = StyleSheet.create({
  button: {
    zIndex: 10,
    left: 20,
    top: 20,
    position: 'absolute',
    backgroundColor: '#0f9eaa',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8
  }
})
