import React, { createContext, useState, useLayoutEffect } from 'react'

const SnackContext = createContext()

export const SnackProvider = ({ children }) => {
  const [state, setState] = useState(false)
  const [text, setText] = useState('')
  const [error, setError] = useState(false)
  const [color, setColor] = useState('')

  useLayoutEffect(() => {
    state
      ? setTimeout(() => {
          setState(!state)
        }, 3000)
      : ''
  }, [state])

  const changeSnackState = (text, err = false, color = 'green') => {
    err ? setError(true) : setError(false)
    setColor(color)
    setState(!state)
    setText(text)
  }

  const value = {
    error,
    state,
    text,
    color,
    changeSnackState
  }

  return <SnackContext.Provider value={value}>{children}</SnackContext.Provider>
}

export default SnackContext
