import React, { memo } from 'react'
import { StyleSheet, View } from 'react-native'
import { CollapsibleItem } from './CollapisibleItem'

const style = StyleSheet.create({
  wrapper: {
    marginBottom: 10,
    alignItems: 'center'
  }
})

const UserItem = ({ ...props }) => {
  return (
    <>
      <View style={style.wrapper}>
        <CollapsibleItem {...props} />
      </View>
    </>
  )
}

export default memo(UserItem)
