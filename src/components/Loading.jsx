import React from 'react'
import { ActivityIndicator, View, StyleSheet } from 'react-native'

export const Loading = () => {
  return (
    <View style={style.box}>
      <ActivityIndicator size={50} color="#305F72" />
    </View>
  )
}

const style = StyleSheet.create({
  box: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
