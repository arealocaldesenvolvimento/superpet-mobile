import * as SQLite from 'expo-sqlite'

const db = SQLite.openDatabase('db.db')

export const createTable = () => {
  db.transaction((tx) => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS number (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)')
  })
}

export const fetchLocalData = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM number',
        null,
        (txObj, { rows: { _array } }) => {
          resolve(_array)
        },
        (txObj, error) => reject(error)
      )
    })
  })
}

export const fetchItem = (value) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM number WHERE value = ?',
        [value],
        (txObj, { rows: { _array } }) => {
          resolve(_array)
        },
        (txObj, error) => reject(error)
      )
    })
  })
}

export const newItem = async (nr_inscricao) => {
  const exists = await fetchItem(nr_inscricao)

  if (exists.length != '') {
    console.log('ja existe')
    return
  } else {
    db.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO number (value) values (?)`,
        [nr_inscricao],
        (txObj, resultSet) => console.log(resultSet),
        (txObj, error) => console.log('Error', error)
      )
    })
  }
}

export const deleteItem = (id) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql('DELETE FROM number WHERE id = ?', [id], (txObj, resultSet) => {
        if (resultSet.rowsAffected > 0) {
          console.log(resultSet.rowsAffected)
          resolve(resultSet)
        }
      })
    })
  })
}
