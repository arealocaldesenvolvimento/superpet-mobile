import React, { useState, useContext, useCallback, useEffect } from 'react'
import { ScrollView, View, Text, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import { BackButtonNav } from '../../components/layout/BackButtonNav'
import { Error } from '../../components/RenderError'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { style } from './style.css'
import { AuthContext } from '../../context/auth'
import { Button } from '../../components/layout/Button'
import { ButtonLoading } from '../../components/layout/ButtonLoading'

import http from '../../api/http'

const Profile = () => {
  const { user } = useContext(AuthContext)

  const [loading, setLoading] = useState(false)
  const [data, setData] = useState(user)

  const handleData = (value, name) => {
    setData({ ...data, [name]: value })
  }

  const fetchUser = useCallback(async () => {
    const response = await http.get(`/users/${user._id}`)
    setData(response.data)
  }, [loading])

  const update = async () => {
    setLoading(true)
    const response = await http.put(`/users/${user._id}`, { ...data })
    setLoading(false)
    console.log(response.data)
  }

  useEffect(() => {
    fetchUser()
  }, [])

  const renderForm = () => {
    return (
      <View style={style.registerContainer}>
        <BackButtonNav />
        <Text style={{ marginTop: 80, color: '#707070', fontSize: 19 }}>Meus dados</Text>

        <View style={style.registerForm}>
          <TextInput
            onChangeText={(value) => handleData(value, 'name')}
            style={style.registerInput}
            placeholderTextColor="#305F72"
            placeholder="nome"
            defaultValue={data.name}
            editable={false}
            selectTextOnFocus={false}
          />

          <TextInput
            style={style.registerInput}
            placeholderTextColor="#305F72"
            placeholder="email"
            onChangeText={(value) => handleData(value, 'email')}
            defaultValue={data.email}
            editable={false}
            selectTextOnFocus={false}
          />

          {loading ? (
            <ButtonLoading>
              <ActivityIndicator size="large" color="#FFF" />
            </ButtonLoading>
          ) : (
            <></>
            // <Button onPress={update}>
            //   <Text style={{ color: '#fff' }}>ATUALIZAR</Text>
            //   <MaterialIcons name="east" size={13} color="#FFF" />
            // </Button>
          )}
        </View>
      </View>
    )
  }

  return <ScrollView>{data ? renderForm() : Error()}</ScrollView>
}

export default Profile
