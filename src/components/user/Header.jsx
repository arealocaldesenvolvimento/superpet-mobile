import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const style = StyleSheet.create({
  header: {
    padding: 10,
    width: Dimensions.get('screen').width - 32,
    backgroundColor: '#009EAA',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 10
  }
})

export const Available = ({ ...props }) => {
  return (
    <TouchableOpacity style={style.header} {...props}>
      <Icon name="user" size={60} color="#a3bdbe" />
      <View style={{ width: '70%' }}>
        <Text style={{ color: '#FFF', fontSize: 20, fontWeight: 'bold' }}>{props.name}</Text>
        <Text style={{ color: '#FFF' }}>veja mais informações</Text>
      </View>
    </TouchableOpacity>
  )
}

export const Unavailable = ({ ...props }) => {
  return (
    <View style={style.header} {...props}>
      <Icon name="user" size={60} color="#a3bdbe" />
      <View style={{ width: '55%' }}>
        <Text style={{ color: '#FFF', fontSize: 20, fontWeight: 'bold' }}>{props.name}</Text>
        <Text style={{ color: '#FFF' }}>visualização bloqueada...</Text>
      </View>
      <Icon name="lock" size={30} color="#a3bdbe" />
    </View>
  )
}
