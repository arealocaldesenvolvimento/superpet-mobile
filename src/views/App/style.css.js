import { StyleSheet, Dimensions } from 'react-native'

export const style = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject
  },

  ImageBackground: {
    width: '100%',
    height: '100%'
  },

  main: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  logo: {
    resizeMode: 'contain',
    width: Dimensions.get('screen').width - 32,
    marginBottom: -80
  },

  content: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  textBox: {
    marginTop: 5,
    marginBottom: 10,
    width: 170,
    color: '#305F72',
    fontSize: 18,
    fontWeight: 'bold'
  },

  cta: {
    width: 170,
    height: 40,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#0055FF8C',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 15
  },

  buttonText: {
    color: '#FFF',
    fontWeight: 'bold'
  },

  /**
   * Profile component
   */

  registerContainer: {
    paddingHorizontal: 30,
    marginBottom: 30
  },

  registerForm: {
    marginTop: 23
  },

  terms: {
    flexDirection: 'row',
    marginBottom: 30
  },

  registerInput: {
    borderWidth: 2,
    borderColor: '#21ABB5',
    paddingVertical: 13,
    paddingHorizontal: 25,
    borderRadius: 10,
    marginBottom: 9
  },

  picker: {
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 2,
    borderColor: '#21ABB5',
    paddingVertical: 13,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginBottom: 9,
    height: 55
  },

  datePicker: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#21ABB5',
    paddingVertical: 13,
    paddingHorizontal: 19,
    borderRadius: 10,
    marginBottom: 9
  },

  // LIST
  modal: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContainer: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 40,
    alignItems: 'center'
  },
  modalTitle: {
    fontSize: 22,
    fontWeight: '600'
  },
  modalText: {
    fontSize: 18,
    color: '#555',
    marginTop: 14,
    textAlign: 'center',
    marginBottom: 10
  },

  buttonText: {
    color: '#fff',
    fontSize: 20
  }
})
